# vidfolderconvert #

This is a simple python script to convert videofiles using ffmpeg. It's optimized for usage on a NAS by Synology and uses some specific functions only available on synology-systems.

## Installation ##

To install the program, simply copy all files of the repo to one directory.

## Usage ##

The program will convert every file in the defined folder matching the defined patterns.

To start the app, simply run `python convert.py` but have a look at the config.ini file first.

Configuration is done using the config.ini file, stored at the same location.
Here you can define the converter-options as well as the path of the videofolder, filepatterns and other options.

Take a look at the example and you will understand.

To make it very easy to control the program without using SSH, you can place txt files in the folder of convert.py.
If the program detects the files, it will execute the desired action and then delete the file.
The file content is not used, so you can simply create an empty file.

Filename | Action
-------- | ------
stop.txt | Stop the encoding after the current file is finished
shutdown.txt | Shutdown system after all files are converted
stop.txt + shutdown.txt | Stop after current encoding has finished and shutdown the system

## Roadmap ##

* optional seperate config-file for every inputfile
* autocrop