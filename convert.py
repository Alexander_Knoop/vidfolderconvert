#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
	vidfolderconvert - Simple python script to convert videos using ffmpeg
	Copyright (C) 2015 Alexander Knoop
	This program comes with ABSOLUTELY NO WARRANTY; for details see the LICENSE file.
	This is free software, and you are welcome to redistribute it under certain conditions; see LICENSE file for details.
	
	https://bitbucket.org/Alexander_Knoop/vidfolderconvert
"""

import sys, os, ConfigParser, subprocess, time, re
import synocontrols as syno
from timetools import Int2Time
from datetime import datetime

# path of script
pfad = os.path.dirname(os.path.realpath(__file__)) + os.sep
config = pfad + 'config.ini'

# load config
parser = ConfigParser.SafeConfigParser()
parser.read(config)
vpfad = parser.get('options', 'path')
beep = parser.getboolean('options', 'beep')

container = parser.get('video', 'container')
vcodec = parser.get('video', 'codec')
crf = parser.get('video', 'quality')
usetune = parser.getboolean('video', 'usetune')
tune = parser.get('video', 'tune')
usefilter = parser.getboolean('video', 'usefilter')
vfilter = parser.get('video', 'filter')

acodec = parser.get('audio', 'codec')
abitrate = parser.get('audio', 'bitrate')

demo = parser.getboolean('options', 'demo')
pattern = parser.get('options', 'pattern')
demostart = parser.get('options', 'demostart')
demolength = parser.get('options', 'demolength')
programm = parser.get('options', 'ffmpeg')

class Video:
	def __init__(self, path, file):
		self.input = file
		self.path = path
		fileName, fileExtension = os.path.splitext(file)
		self.ext = fileExtension
		if fileExtension.upper() == '.MP4':
			self.output = fileName + '.enc.mp4'
		else:
			self.output = fileName + '.mp4'
		self.status = 'new'

def Ordner(directory):
	# creates a folder if it doesn' t exists
	if not os.path.exists(directory):
		os.makedirs(directory)

def decode(Vid):
	print 'Encoding ' + Vid.input
	
	befehl = [
		programm,
		'-i', Vid.path + os.sep + Vid.input,
		'-f', container,
		'-c:v', vcodec,
		'-crf', crf,
		'-c:a', acodec,
		'-b:a', abitrate,
		]

	if usefilter:
		befehl.append('-vf')
		befehl.append(vfilter)
	if usetune:
		befehl.append('-tune')
		befehl.append(tune)
	if demo:
		befehl.append('-ss')
		befehl.append(demostart)
		befehl.append('-t')
		befehl.append(demolength)

	befehl.append(Vid.path + os.sep + Vid.output)

	p = subprocess.call(befehl, stdout=subprocess.PIPE)

	print 'Finished encoding'
	if p == 0:
		# No errors
		syno.beep2('short', beep)
		Vid.status = 'converted'
	else:
		# Errors
		syno.beep2('long', beep, 2)
		Vid.status = 'failed'
	return Vid

def get_file(dir):
	# Returns a valid file to convert
	ret = None
	for x in os.listdir(vpfad):
		if re.match(pattern, x):
			ret = Video(dir, x)
			break
	return ret

syno.status('oblink') # Anzeigen, dass gearbeitet wird
syno.beep2('short', beep)
infotext = 'No videos found.'

while True:
	# Abort
	if os.path.isfile(pfad + 'stop.txt'):
		infotext = 'Encoding aborted'
		print infotext
		os.remove(pfad + 'stop.txt')
		break
	
	Vid = get_file(vpfad)
	if not Vid:
		infotext = 'No more files available.'
		break
	
	tstart = datetime.now()
	syno.send_message('Encoding', Vid.input)
	Vid = decode(Vid)
	tende = datetime.now()
		
	print Int2Time((tende-tstart).total_seconds())
	
	# move files
	if Vid.status == 'converted':
		syno.send_message('Encoding successful', Int2Time((tende-tstart).total_seconds()) + ' - ' + Vid.input)
		Ordner(Vid.path + os.sep + 'encoded')
		os.rename(Vid.path + os.sep + Vid.input, Vid.path + os.sep + 'encoded' + os.sep + Vid.input)
		os.rename(Vid.path + os.sep + Vid.output, Vid.path + os.sep + 'encoded' + os.sep + Vid.output)
	else:
		syno.send_message('An error occurred', Int2Time((tende-tstart).total_seconds()) + ' - ' + Vid.input)
		Ordner(Vid.path + os.sep + 'error')
		os.rename(Vid.path + os.sep + Vid.input, Vid.path + os.sep + 'error' + os.sep + Vid.input)
		os.rename(Vid.path + os.sep + Vid.output, Vid.path + os.sep + 'error' + os.sep + Vid.output)

syno.status('gon')
syno.send_message('Encoding finished', infotext)
time.sleep(1)
syno.beep2('short', beep, 3)

if os.path.isfile(pfad + 'shutdown.txt'):
	print 'Shutting system down'
	os.remove(pfad + 'shutdown.txt')
	syno.send_message('Shutting system down', infotext)
	syno.beep2('long', beep, 2)
	syno.power('blink')
	os.system('poweroff')