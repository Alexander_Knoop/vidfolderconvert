﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
	synocontrols - Python module to control LEDs and more on Synology-NAS
	Copyright (C) 2015 Alexander Knoop
	This program comes with ABSOLUTELY NO WARRANTY; for details see the LICENSE file.
	This is free software, and you are welcome to redistribute it under certain conditions; see LICENSE file for details.
	
	https://bitbucket.org/Alexander_Knoop/synocontrols
"""

import os
from subprocess import call
from time import sleep

msgpath = '/usr/syno/bin/synodsmnotify'

def send_message(title, text):
	send_message2(title, text, 'administrators')
	
def send_message2(title, text, group):
	call([msgpath, '@' + group, title, text])

def dscall(wert):
	os.system('echo \"' + str(wert) + '\" >/dev/ttyS1')

def alarm(duration):
	call(['/tmp/synoschedtool','--beep',str(duration)])

def beep(arg, times=1):
	if arg=='short':
		dscall(2)
		for i in range(times-1):
			sleep(0.5)
			dscall(2)
	elif arg=='long':
		dscall(3)
		for i in range(times-1):
			sleep(1)
			dscall(3)
	else:
		print 'Unknown Command: ' + arg
		
def beep2(arg, active, times=1):
	if active:
		beep(arg, times)
		
def power(arg):
	if arg=='on':
		dscall(4)
	elif arg=='off':
		dscall(6)
	elif arg=='blink':
		dscall(5)
	else:
		print 'Unknown Command: ' + arg
		
def status(arg):
	if arg=='gon':
		dscall(8)
	elif arg=='off':
		dscall(7)
	elif arg=='gblink':
		dscall(9)
	elif arg=='oon':
		dscall(':')
	elif arg=='oblink':
		dscall(';')
	else:
		print 'Unknown Command: ' + arg

def copy(arg):
	if arg=='off':
		dscall('B')
	elif arg=='on':
		dscall('@')
	elif arg=='blink':
		dscall('A')
	else:
		print 'Unknown Command: ' + arg