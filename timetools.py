﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
	timetools - simple python collection of usable timefunctions
	Copyright (C) 2015 Alexander Knoop
	This program comes with ABSOLUTELY NO WARRANTY; for details see the LICENSE file.
	This is free software, and you are welcome to redistribute it under certain conditions; see LICENSE file for details.
	
	https://bitbucket.org/Alexander_Knoop/timetools
"""

def Time2Int(timestring):
	# Converts a timestring ('09:13:57') in seconds
	# counterpart of Int2Time
	s = timestring.split(':')
	t = int(s[0])*3600 + int(s[1])*60 + int(s[2])
	return t

def setlength(value,digits):
	# subfunction to convert integers to string in special length
	# input: value = 67, digits=3
	# output: 067
	r = str(value)
	for i in range(len(str(value)), digits, 1):
		r = '0' + r
	return r

def Int2Time(t):
	# converts seconds to timestring ('09:13:57')
	# counterpart of Time2Int
	h = int(t/3600)
	t = t - h*3600
	m = int(t/60)
	t = t - m*60
	return setlength(h,2) + ':' + setlength(m,2) + ':' + setlength(t,2)

def addTime(timestring, Offset):
	# adds Offset seconds to the given timestring ('09:13:57') and returns a new timestring
	t = Time2Int(timestring) + Offset
	return Int2Time(t)
